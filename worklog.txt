I am starting to work through Ray Tracing in One Weekend.

After reading the Chapter 1 Overview, I have gotten known the general outlook of the book.

RTIOW: 2.1 THE PPM Image Format: Studied how to see an image.

RTIOW: 2.2 Creating an Image File: Studied how to use cmake to build c/c++ file and how to create an image.

RTIOW: 2.3 Adding a Progress Indicator: Learned how to handily track the progress of a long render.

RTIOW: 3.1 Variables and Methods: Changed the sturcture of folders and finished first part of vec3.h.

RTIOW: 3.2 vec3 Utility Functions: Finished the utility functions.

RTIOW: 3.3 Color Utility Functions: Using color utility functions to abstract and debrief my main.cc.

RTIOW: 4.1 The ray Class: Finished the ray class.

RTIOW: 4.2 Sending Rays Into the Scene: Changed the main.cc program, and studied how to send rays into the scene.

RTIOW: 5.1 Ray-Sphere Intersection: Got known the mathematical theory of ray-sphere intersection.

RTIOW: 5.2 Creating Our First Raytraced Image: Successufully created a simple red sphere.

RTIOW: 6.1 Shading with Surface Normals: Added shadow on a sphere.

RTIOW: 6.2 Simplifying the Ray-Sphere Intersection Code: I just changed the formation of some equations, which makes the code briefer.

RTIOW: 6.3 An Abstraction for Hittable Objects: Created two h files to abstrct the hittable objects

RTIOW: 6.4 Front Faces Versus Back Faces:
       	   1. Strengh my undersanding of struct
	   2. Considered the side of the surface

RTIOW: 6.5 A List of Hittable Objects:
       	   1. Created a list to store hittable objects
	   2. Used some C++ features

RTIOW: 6.6 Some New C++ Features:
       	   1. Reviewed C++ features that I used in the last section

RTIOW: 6.7 Common Constants and Utility Functions:
       	   1. Trew throw common useful constants and future utility functions in rtweekend.h
	   2. Modified the main.cc
	   3. Created the Image 5(image6-7.pmm)

RTIOW: 7.1 Some Random Number Utilities:
       	   1. Added random functions into rtweekend.h
	   2. The textbook provids an alternative function which uses some features of C++, but I have not chosen it

RTIOW: 7.2 Generating Pixels with Multiple Samples:
       	   1. Created a camera class to handle virtual camera and other related tasks of scene scampling
	   2. Added clamp function into rtweekend.h
	   3. Changed color() function into the multi-smaple write
	   4. Changed CMakelist , debuged and built 
	   5. Created an image with less edge pixels

RTIOW: 8.1 A Simple Diffuse Material:
       	   1. Added random utility functions and the random_in_unit_sphere() function into vec3.h, which could pick a random point in a unit radius sphere
	   2. Updated the ray_color() function in main.cc to use the new random direction generator

RTIOW: 8.2 Limiting the Number of Child Rays:
       	   1. Updated the ray_color() function to stop recursing
	   2. Built and debuged, and generated the Image7(image8-2.ppm)

RTIOW: 8.3 Using Gamma Correction for Accurate Color Intensity:
       	   1. Updated the color.h and used a new way to divide colors
	   2. Generated the Image8(image8-3.ppm), with gamma correction, which is brighter

RTIOW: 8.4 Fixing Shadow Acne:
       	   1. Fixed a subtle bug about tolerance

RTIOW: 8.5 True Lambertian Reflection:
       	   1. Added the random_unit_vector() function into vec3.h
	   2. Updated main.cc using the above function
	   3. Built and created a new image, which is less pronounced and lighter

RTIOW: 8.6 An Alternative Diffuse Formulation:
       	   1. Tried another way to created a similar image with image8-5

RTIOW: 9.1 An Abstract Class for Materials:
       	   1. Created an abstract class material in material.h

RTIOW: 9.2 A Data Structure to Describe Ray-Object Intersections:
       	   1. Added material class into hit_record struct in hittable.h
	   2. Added material information into ray-sphere intersection

RTIOW: 9.3 Modeling Light Scatter and Reflectance:
       	   1. Created the lambertian material class
	   2. Added near_zero() method into vec3.h
	   3. Updated the material.h to catch degenerate scatter direction

RTIOW: 9.4 Mirrored Light Reflection:
       	   1. Added reflect() function into vec3.h
	   2. Created metal class using reflect() function
	   3. Updated the main.cc

RTIOW: 9.5 A Scene with Metal Spheres:
       	   1. Updated main.cc
	   2. Built and debuged
	   3. Created the Image 11(image9-5.ppm)

RTIOW: 9.6 Fuzzy Reflection:
       	   1. Added some fuzziness feature into material.h
	   2. Updated main.cc and created Image 12(image9-6.ppm)

RTIOW: 10.1 Refraction:
       	   1. Got known the problem of the refracted ray

RTIOW: 10.2 Snell's Law:
       	   1. Overloaded refraction function in vec3.h
	   2. Added dielectric class in material.h
	   3. Updated main.cc and created Image 14(image10-2.ppm)

RTIOW: 10.3 Total Internal Reflection:
       	   1. Wrote some code to determine if the ray can refract
	   2. Updated the dielectric material which always refracts
	   3. Built and created Image 15(image10-3.ppm)

RTIOW: 10.4 Schlick Approximation:
       	   1. Updated the dielectirc material

RTIOW: 10.5 Modeling a Hollow Glass Sphere:
       	   1. Updated main.cc
	   2. Created the image of a hallow glass sphere

RTIOW: 11.1 Camera Viewing Geometry:
       	   1. Created adjustable field-of-view in camera
	   2. Updated main.cc and created image11-1.ppm
	   
RTIOW: 11.2 Positioning and Orienting the Camera:
       	   1. Changed some elements in camera.h and main.cc
	   2. Viewed the object from different positions and distances
	   3. Created image11-2-1.ppm and image11-2-2.ppm

RTIOW: 12.1 A Thin Lens Approximation:
       	   1. Reviewed some fundamental knowledge about geometric optics
	   2. By the way, I was admitted by my undergraduate university through physics competitions, so this part of knowledge brings back memories of my high school life:)

RTIOW: 12.2 Generating Sample Rays:
       	   1. Made some adjustments in camera.h and main.cc to observe with depth-of-field
	   2. Write a random point function inside unit dist in vec3.h
	   3. Created image12-2.ppm
	   
RTIOW: 13.1 A Final Render:
       	   1. Updated the main.cc
	   2. Greated image13-1.ppm, which took my laptop 1 night
	   3. Finished the extra credit assignment!!!
	   :)
